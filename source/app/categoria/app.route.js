(function () {
    'use strict';

    angular.module('app.categoria.router', [

    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('categoria', {
                url: '/categoria',
                views: {
                    'categoria': {
                        template: '<categoria/>'
                    },
                    'piepagina': {
                        template: '<piepagina/>'
                    },
                    'encabezado': {
                        template: '<encabezado/>'
                    }
                }
            });
    };
})();
