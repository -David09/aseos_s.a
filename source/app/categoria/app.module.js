(function (){
    'use strict';

    angular.module('app.categoria', [
        'app.categoria.router',
        'app.categoria.directivas',
        'app.categoria.controller'
    ]);

})();
