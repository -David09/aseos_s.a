(function () {
    'use strict';

    angular.module('app.categoria.directivas', [

    ]).directive('categoria', categoria)

    function categoria() {
        return {
            scope: {},
            templateUrl: 'app/categoria/categoria.html',
            controller: 'categoriaCtrl',
            controllerAs: 'vm'
        }
    }

})();
