(function () {
    'use strict';

    angular.module('app.categoria.controller', [
    ])
    .controller('categoriaCtrl', categoriaCtrl)


    function categoriaCtrl() {
        this.categoria = [
            {nombre: 'Aseo Industrial'},
            {nombre: 'Aseo Hogar'},
            {nombre: 'Aseo Personal'},
            {nombre: 'Limpieza'}
        ]

    }
})();
