(function () {
    'use strict';

    angular.module('app.config', [
    ]).constant('BASEURL', 'http://localhost:8080/portafolio-backend/webresources/');

    //En este módulo se pueden declarar y asignar todas las constantes
    //que se usarán en la aplicación.

})();
