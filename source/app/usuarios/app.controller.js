(function() {
  'use strict';

  angular.module('app.usuarios.controller', [

  ]).controller('usuariosCtrl', usuariosCtrl);

  usuariosCtrl.$inject = ['Usuarios'];

  function usuariosCtrl(Usuarios){
    this.usuarios = Usuarios.query();
  }

})();
