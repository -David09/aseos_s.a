  (function () {
    'use strict';
    angular.module('app.usuarios.services', [

    ])
        .factory('Usuarios', Usuarios);

    Usuarios.$inject = ['$resource', 'BASEURL'];

    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function Usuarios($resource, BASEURL) {
        return $resource(BASEURL + 'usuarios/:id',
        //return $resource(BASEURL+'/categorias/:id'
        //Se debe digitar el id del modelo
        //{ inmuebleId: '@_id' },
          {
            id: '@id'
          }, {
            update: {
              method: 'PUT'
          }
        }
      )
    }

    //De igual manera los factory nos sirven para almacenar información
    //y que nos pueda servir en cualquier controlador o lugar de la aplicación
    //evitando de esta manera hacer variables globales.
})();
