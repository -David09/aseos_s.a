(function() {
    'use strict';

    angular.module('app.usuarios.directivas', [

    ]).directive('usuarioslist', usuarioslist)
    .directive('usuarioscreate', usuarioscreate);

    function usuarioslist() {
        return {
            scope: {},
            templateUrl: 'app/usuarios/lista.html',
            controller: 'usuariosCtrl',
            controllerAs: 'vm'
        }
    }
    function usuarioscreate(){
      return{
        scope:{},
        templateUrl: 'app/usuarios/create.html',
        controller: 'usuariosCtrl',
        controllerAs: 'vm'
      }
    }
})();
