(function () {
    'use strict';

    angular.module('app.usuarios.router', [
        'app.usuarios.controller'
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('usuarioslist', {
                url: '/usuarios',
                views: {
                    'usuarioslist': {
                        template: '<usuarioslist/>'

                    },
                    'piepagina': {
                        template: '<piepagina/>'
                    },
                    'encabezado': {
                        template: '<encabezado/>'
                    }

                }
            })
            .state('usuarioscreate', {
                url: '/usuarios/create',
                views: {
                    'usuarioscreate': {
                        template: '<usuarioscreate/>'

                    },
                    'piepagina': {
                        template: '<piepagina/>'
                    },
                    'encabezado': {
                        template: '<encabezado/>'
                    }

                }
            });
    };
})();
